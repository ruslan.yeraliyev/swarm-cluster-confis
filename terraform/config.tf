variable "os-image" {
  default = "ubuntu-20.04"
}

variable "swarm-manager-type" {
  default = "cx11"
}

variable "count-swarm-secondary-managers" {
  default = 1
}

variable "swarm-worker-type" {
  default = "cx31"
}

variable "count-swarm-workers" {
  default = 2
}

variable "hcloud_token" {}


data "hcloud_datacenter" "nuremberg" {
  name = "nbg1-dc3"
}

data "hcloud_ssh_key" "haarvester" {
  name = "haarvester@haarvester"
}